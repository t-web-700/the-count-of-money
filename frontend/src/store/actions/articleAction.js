import * as types from '../constant'
import authServices from '../../services/AuthServices/authServices'
import axios from 'axios'
import {toast} from 'react-toastify'
import history from "../../history";
import userServices from "../../services/UserServices/userServices";
import articleServices from "../../services/ArticleServices/articleServices";

export const getArticles = (excludedTags, includedTags) => async dispatch => {
    await articleServices.getArticles(excludedTags, includedTags).then(
        (success) => {
            dispatch({type: types.GET_ARTICLES_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.GET_ARTICLES_FAIL})
        })
}

export const getArticle = (id) => async dispatch => {
    await articleServices.getArticle(id).then(
        (success) => {
            dispatch({type: types.GET_ARTICLE_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.GET_ARTICLE_FAIL})
        })
}