import * as types from '../constant'

export function action1(){
    return {
        type: types.ACTION_TEST1
    }
}

export function action2 (){
    return {
        type: types.ACTION_TEST2
    }
}