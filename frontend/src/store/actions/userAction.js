import * as types from '../constant'
import authServices from '../../services/AuthServices/authServices'
import axios from 'axios'
import {toast} from 'react-toastify'
import history from "../../history";
import userServices from "../../services/UserServices/userServices";

export const getProfile = () => async dispatch => {
    await userServices.getProfile().then(
        (success) => {
            dispatch({type: types.GET_PROFILE_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'});
            dispatch({type: types.GET_PROFILE_FAIL})
        })
}

export const updateProfile = (username, email, currency, cryptos, keywords) => async dispatch => {
    await userServices.updateProfile(username, email, currency, cryptos, keywords).then(
        (success) => {
            toast('Profil mis à jour avec succès', {type: 'info'})
            dispatch({type: types.UPDATE_PROFILE_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.UPDATE_PROFILE_FAIL})
        })
}