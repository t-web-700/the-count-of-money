import * as types from '../constant'
import authServices from '../../services/AuthServices/authServices'
import axios from 'axios'
import {toast} from 'react-toastify'
import history from "../../history";
import cryptoServices from "../../services/CryptoServices/cryptoServices";

export const getCryptos = (cmids) => async dispatch => {
    await cryptoServices.getCryptos(cmids).then(
        (success) => {
            dispatch({type: types.GET_CRYPTOS_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'});
            dispatch({type: types.GET_CRYPTOS_FAIL})
        })
}

export const getCrypto = () => async dispatch => {
    await cryptoServices.getCrypto().then(
        (success) => {
            dispatch({type: types.GET_CRYPTO_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.GET_CRYPTO_FAIL})
        })
}

export const getHistoricalCrypto = (cmid, period) => async dispatch => {
    await cryptoServices.getHistoricalCrypto(cmid, period).then(
        (success) => {
            dispatch({type: types.GET_CRYPTO_HISTORICAL_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.GET_CRYPTO_HISTORICAL_FAIL})
        })
}

export const sendCrypto = (code, name, img, cmid) => async dispatch => {
    await cryptoServices.sendCrypto(code, name, img, cmid).then(
        (success) => {
            dispatch({type: types.SEND_CRYPTO_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.SEND_CRYPTO_FAIL})
        })
}

export const deleteCrypto = (cmid) => async dispatch => {
    await cryptoServices.deleteCrypto(cmid).then(
        (success) => {
            dispatch({type: types.DELETE_CRYPTO_SUCCESS, payload: success })
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.DELETE_CRYPTO_SUCCESS})
        })
}