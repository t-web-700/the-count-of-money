import * as types from '../constant'
import authServices from '../../services/AuthServices/authServices'
import axios from 'axios'
import {toast} from 'react-toastify'
import history from "../../history";
import {getProfile} from "./userAction";

export const login = (email, password) => async dispatch => {
    await authServices.login(email, password).then(
        (success) => {
            dispatch({type: types.AUTH_LOGIN_SUCCESS, payload: success })
            dispatch(getProfile())
            history.push('/')
        },
        (error) => {
            toast(error, {type: 'error'});
            dispatch({type: types.AUTH_LOGIN_FAIL})
        })
}

export const logout = () => async dispatch => {
    await authServices.logout().then(
        (success) => {
            if (axios.defaults.headers.common['Authorization']) {
                delete axios.defaults.headers.common['Authorization']
            }
            if (localStorage.getItem('user')) {
                localStorage.removeItem('user')
                window.location.reload();
            }
            toast('Vous avez été deconnecté avec succès !', {type: 'info'})
            history.push('/')
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.AUTH_REGISTER_FAIL})
            history.push('/')
        })
}

export const register = (username, email, password, repassword) => async dispatch => {
    await authServices.register(username, email, password, repassword).then(
        (success) => {
            dispatch({type: types.AUTH_REGISTER_SUCCESS, payload: success })
            dispatch(getProfile())
            history.push('/')
        },
        (error) => {
            toast(error, {type: 'error'})
            dispatch({type: types.AUTH_REGISTER_FAIL})
        })
}

export const loginYet = () => async dispatch => {
    dispatch({type: types.AUTH_LOGIN_YET})
}