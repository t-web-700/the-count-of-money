const initialState = {
    login_status: '',
    register_status: '',
    isAuth: false,
    token: (JSON.parse(localStorage.getItem('user'))) ? JSON.parse(localStorage.getItem('user')).token : ''
}

function authReducer (state = initialState, action) {
    switch(action.type) {
        case 'AUTH_LOGIN_SUCCESS':
            return {
                ...state,
                login_status: 'success',
                isAuth: true,
                token: action.payload.token
            }
        case 'AUTH_LOGIN_FAIL':
            return {
                ...state,
                isAuth: false,
                login_status: 'error'
            }
        case 'AUTH_LOGOUT_SUCCESS':
            return {
                ...state,
                isAuth: false,
                token: ''
            }
        case 'AUTH_LOGOUT_FAIL':
            return {
                ...state
            }
        case 'AUTH_REGISTER_SUCCESS':
            return {
                ...state,
                register_status: 'success',
                isAuth: true,
                token: action.payload.token
            }
        case 'AUTH_REGISTER_FAIL':
            return {
                ...state,
                isAuth: false,
                register_status: 'error'
            }
        case 'AUTH_LOGIN_YET':
            return {
                ...state,
                isAuth: true,
                token: JSON.parse(localStorage.getItem('user'))
            }
        default:
            return state
    }
}

export default authReducer