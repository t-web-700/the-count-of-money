const initialState = {
    cryptos:[],
    historical:{}
}

function cryptoReducer (state = initialState, action) {
    switch(action.type) {
        case 'GET_CRYPTOS_SUCCESS':
            return {
                ...state,
                cryptos: action.payload
            }
        case 'GET_CRYPTOS_FAIL':
            return {
                ...state,
                cryptos: []
            }
        case 'GET_CRYPTO_SUCCESS':
            return {
                ...state
            }
        case 'GET_CRYPTO_FAIL':
            return {
                ...state
            }
        case 'GET_CRYPTO_HISTORICAL_SUCCESS':
            return {
                ...state,
                historical: action.payload
            }
        case 'GET_CRYPTO_HISTORICAL_FAIL':
            return {
                ...state,
                historical: []
            }
        case 'SEND_CRYPTO_SUCCESS':
            return {
                ...state
            }
        case 'SEND_CRYPTO_FAIL':
            return {
                ...state
            }
        case 'DELETE_CRYPTO_SUCCESS':
            return {
                ...state
            }
        case 'DELETE_CRYPTO_FAIL':
            return {
                ...state
            }
        default:
            return state
    }
}

export default cryptoReducer