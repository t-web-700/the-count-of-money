const testState = {
    test: 42
}

function testReducer (state = testState, action) {
    switch(action.type) {
        case 'ACTION_TEST1':
            return {
                ...state,
                test: state.test + 1
            }
        case 'ACTION_TEST2':
            return {
                ...state,
                test: state.test - 1
            }
        default:
            return state
    }
}

export default testReducer