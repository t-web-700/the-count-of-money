const initialState = {
    articles_status: '',
    article_status: '',
    articles: [],
    article: {}
}

function articleReducer (state = initialState, action) {
    switch(action.type) {
        case 'GET_ARTICLES_SUCCESS':
            return {
                ...state,
                articles_status: 'success',
                articles: action.payload
            }
        case 'GET_ARTICLES_FAIL':
            return {
                ...state,
                articles_status: 'error'
            }
        case 'GET_ARTICLE_SUCCESS':
            return {
                ...state,
                article_status: 'success',
                article: action.payload
            }
        case 'GET_ARTICLE_FAIL':
            return {
                ...state,
                article_status: 'error',

            }
        default:
            return state
    }
}

export default articleReducer