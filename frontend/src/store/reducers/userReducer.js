const initialState = {
    user_status: '',
    user: {}
}

function authReducer (state = initialState, action) {
    switch(action.type) {
        case 'GET_PROFILE_SUCCESS':
            return {
                ...state,
                user_status: 'success',
                user: action.payload
            }
        case 'GET_PROFILE_FAIL':
            return {
                ...state,
                user_status: 'error'
            }
        case 'UPDATE_PROFILE_SUCCESS':
            return {
                ...state,
                user: action.payload
            }
        case 'UPDATE_PROFILE_FAIL':
            return {
                ...state
            }
        default:
            return state
    }
}

export default authReducer