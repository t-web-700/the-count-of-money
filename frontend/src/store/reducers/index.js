import { combineReducers } from 'redux'
import testReducer from "./testReducer"
import authReducer from "./authReducer"
import cryptoReducer from "./cryptoReducer";
import userReducer from "./userReducer";
import articleReducer from "./articleReducer";

//Import and put in combine Reducer
export default combineReducers({
    testReducer,
    authReducer,
    cryptoReducer,
    userReducer,
    articleReducer
})