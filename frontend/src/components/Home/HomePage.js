import React, { Component } from 'react'
import {connect} from 'react-redux'
import { getCryptos, getArticles } from '../../store/actions'
import { HashLink as Link } from 'react-router-hash-link'
import Scrollchor from 'react-scrollchor'
import history from '../../history'
import {toast} from 'react-toastify'

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.props.getCryptos([])
        this.props.getArticles('','')
    }
    render(){
        return (
            <main>
                <header id="homepage-header" className="spaced shrinked bg-gradient flex-col justify-center">
                    <h1 className="white"><span>Bienvenue sur</span><br/>Platform</h1>
                    <p className="white">Restez informé des dernières tendances de vos cryptomonnaies</p>
                    <div className="page-links">
                        <Scrollchor to="#cryptocurrencies" className="btn">Cryptomonnaies</Scrollchor>
                        <Scrollchor to="#news" className="btn">News</Scrollchor>
                    </div>
                </header>
                <section id="cryptocurrencies" className="spaced shrinked">
                    <h2>Cryptomonnaies</h2>
                    <div id="crypto-table" className="shadowed">
                        <table>
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Code</th>
                                <th>Valeur</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.cryptos.map((r, i) => {
                                return (
                                    <tr key={i} onClick={() => {
                                        this.props.isAuth ?
                                            history.push('/crypto/' + r.id) :
                                            toast("Vous devez être connecté pour accéder à l'historique de " +
                                                "la cryptomonnaie selectionné", {type: 'error'});
                                    }} className='cryptoHover'>
                                        <td>{r.name}</td><td>{r.code}</td><td>{r.price}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                </section>
                <section id="news" className="spaced shrinked">
                    <h2>News</h2>
                    <div id="news-display" className="flex justify-between wrap">
                        {/*boucle ici*/}
                        {this.props.articles.slice(0,3).map((r, i) => {
                            const date = new Date(r.date)
                            const description = r.description.substring(0, 150) + " (...)";
                            return (
                                <Link to={'/news/' + r._id} className="flex-col shadowed">
                                    <img src="https://via.placeholder.com/300X150?text=Placeholder" alt="Image1" />
                                    <div className="news-infos">
                                        <span className="date">{date.getDate()}/{date.getMonth() + 1}/{date.getFullYear()} à {date.getHours()}:{date.getMinutes()}</span>
                                        <h3>{r.title}</h3>
                                        <p>{description}</p>

                                    </div>
                                </Link>
                            )
                        })}
                    </div>
                    <button id="show-news" className="btn" >Plus de news</button>
                </section>
            </main>
        )
    }
}

function mapStateToProps(state) {
    return {
        cryptos: state.cryptoReducer.cryptos,
        profile: state.userReducer.user,
        isAuth: state.authReducer.isAuth,
        articles: state.articleReducer.articles
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getCryptos: (cmids) => dispatch(getCryptos(cmids)),
        getArticles: (excludedTags, IncludedTags) => dispatch(getArticles(excludedTags, IncludedTags))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
