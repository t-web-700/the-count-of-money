import React, { Component } from 'react'
import {login} from "../../store/actions/authAction";
import {connect} from "react-redux";

class LoginPart extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        let { email, password } = this.state;
        this.props.login(email, password);
        this.setState({
            email: '',
            password: ''
        });
    }

    render(){
        let {email, password} = this.state;
        return (
            <form onSubmit={this.onSubmit} className="bg-white flex-col justify-center align-center">
                <h1>Connexion</h1>
                <input type="email" name="email" onChange={e => this.setState({email: e.target.value})} value={email} placeholder="Adresse mail" />
                <input type="password" name="password" onChange={e => this.setState({password: e.target.value})} value={password} placeholder="Mot de passe" />
                <input type="submit" className="btn" value='Se connecter'></input>
            </form>
        )
    }
}


function mapStateToProps(state) {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: (email, password) => dispatch(login(email, password))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPart)
