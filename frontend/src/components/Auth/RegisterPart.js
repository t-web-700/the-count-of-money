import React, { Component } from 'react'
import {login, register} from "../../store/actions";
import {connect} from "react-redux";

class RegisterPart extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        let { username, email, password, repassword } = this.state
        this.props.register(username, email, password, repassword)
    }

    render(){
        let {username, email, password, repassword} = this.state
        return (
            <form onSubmit={this.onSubmit} className="bg-white flex-col justify-center align-center">
                <h1>Inscription</h1>
                <input type="text" name="username" onChange={e => this.setState({username: e.target.value})} value={username} placeholder="Nom d'utilisateur" />
                <input type="email" name="email" onChange={e => this.setState({email: e.target.value})} value={email} placeholder="Adresse mail" />
                <input type="password" name="password" onChange={e => this.setState({password: e.target.value})} value={password} placeholder="Mot de passe" />
                <input type="password" name="repassword" onChange={e => this.setState({repassword: e.target.value})} value={repassword} placeholder="Retaper mot de passe" />
                <input type='submit' className="btn" value="S'enregistrer"/>
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        register: (username, email, password, repassword) => dispatch(register(username, email, password, repassword))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPart)