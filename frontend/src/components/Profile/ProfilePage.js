import React, { Component } from 'react'
import {connect} from 'react-redux'
import {getProfile, updateProfile} from "../../store/actions";

class ProfilePage extends Component {

    constructor(props) {
        super(props);
        this.props.getProfile()
        this.state = {
        }
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        let { username, email, currency, cryptos, keywords } = this.state;
        this.props.updateProfile(username, email, currency, cryptos, keywords);
    }

    render(){
        let { username, email, currency, cryptos, keywords } = this.state;
        return (
            <main id="news">
                <header className="spaced shrinked bg-gradient white">
                </header>
                <section id="cryptocurrencies" className="spaced shrinked">
                    <h2>Mon Profil</h2>
                    <div className='profile-container'>
                        <form onSubmit={this.onSubmit} className="bg-white flex-col justify-center align-center">
                            <input type="text" name="username" onChange={e => this.setState({username: e.target.value})} value={username} placeholder="Username" />
                            <input type="email" name="email" onChange={e => this.setState({email: e.target.value})} value={email} placeholder="Email" />
                            <select name='currency' onChange={e => this.setState({currency: e.target.value})}>
                                <option value="euros">Euros €</option>
                                <option value="usd">USD $</option>
                            </select>
                            <select className='select-profil' name='cryptos' onChange={e => this.setState({cryptos: e.target.value})} multiple>
                                <option value="volvo">Volvo</option>
                            </select>
                            <select className='select-profil' name='keywords' onChange={e => this.setState({keywords: e.target.value})} multiple>
                                <option value="volvo">Volvo</option>
                            </select>
                            <input type="submit" className="btn" value='Mettre à jour mon profil'></input>
                        </form>
                    </div>
                </section>
            </main>
        )
    }
}

function mapStateToProps(state) {
    return {
        profile: state.userReducer.profile
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getProfile: () => dispatch(getProfile()),
        updateProfile: (username, email, currency, cryptos, keywords) => dispatch(updateProfile(username, email, currency, cryptos, keywords))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage)