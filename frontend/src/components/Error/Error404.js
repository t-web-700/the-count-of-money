import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'

class Error404 extends Component {
    render(){
        return (
	    <main id="page-not-found" className="bg-gradient flex-col justify-center align-center text-center white">
	      <h1>404<br/><span>Page non trouvée</span></h1>
	      <p>La page que vous recherchez n'existe pas ou plus.</p>
	      <Link to="/home" className="white">Retourner à l'accueil</Link>
	    </main>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Error404)
