import React, { Component } from 'react'
import {connect} from 'react-redux'
import CryptoChart from "../Chart/CryptoChart";
import {getCryptos, getHistoricalCrypto} from '../../store/actions'
import loader from '../../assets/img/loader.gif'

class CryptoPage extends Component {
    constructor(props) {
        super(props);
        this.props.getCryptos([])
        this.props.getHistoricalCrypto(this.props.match.params.id, 'minute')
        this.state = {
            loader: true,
            current: 'minute',
            config: {
                title: '',
                backgroundColor: 'rgba(35,155,86,.5)',
                borderColor: 'rgb(30,132,73)',
                data: [],
                labels: []
            }
        }
    }

    checkNumberDigits(myNumber)
    {
        return (myNumber < 10 ? "0" : "") + myNumber;
    }

    handleClickSelected = (current) => this.setState(prevState => ({
        ...prevState,
        current: current
    }))

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.state.loader = true
        if(prevProps.crypto !== this.props.crypto){
            this.setState({
                config:{
                    ...prevState.config,
                    title: this.props.crypto.name
                }
            })
        }
        if(prevProps.historical !== this.props.historical){
            let labels = this.props.historical.intervalData.map(a => {
                const date = new Date(a.date)
                return this.checkNumberDigits(date.getDate()) + '/' +
                    this.checkNumberDigits(date.getMonth() + 1) + '/' +
                    date.getFullYear() + ' ' +
                    this.checkNumberDigits(date.getHours()) + ':' +
                    this.checkNumberDigits(date.getMinutes()) + ':' +
                    this.checkNumberDigits(date.getSeconds())
            })
            let data = this.props.historical.intervalData.map(a => a.price)
            this.setState({
                config:{
                    ...prevState.config,
                    data: data,
                    labels: labels
                }
            })
        }
    }

    render(){
        return (
            <main id="news">
                <header className="spaced shrinked bg-gradient white">
                </header>
                <div className='button-chart'>
                    <span className={(this.state.current === 'daily' ? 'selected' : '')} onClick={() => {this.props.getHistoricalCrypto(this.props.match.params.id, 'daily');this.handleClickSelected('daily')}}>days</span>
                    <span className={(this.state.current === 'hourly' ? 'selected' : '')} onClick={() => {this.props.getHistoricalCrypto(this.props.match.params.id, 'hourly');this.handleClickSelected('hourly')}}>hours</span>
                    <span className={(this.state.current === 'minute' ? 'selected' : '')} onClick={() => {this.props.getHistoricalCrypto(this.props.match.params.id, 'minute');this.handleClickSelected('minute')}}>minutes</span>
                </div>
                <div className='infos-chart-container'>
                    <div className='infos-chart'>
                        <span><b>Highest : </b>{this.props.historical.highest}</span>
                        <span><b>Lowest : </b>{this.props.historical.lowest}</span>
                        <span><b>Currency : </b>{this.props.historical.currency}</span>
                    </div>
                </div>
                <div>
                    <CryptoChart className='cryptoChart' id={'chart'} height={600} config={this.state.config}></CryptoChart>
                    {
                        /*this.state.loader ? <div className='loader-container'><img src={loader}/></div> : ''*/
                    }
                </div>
            </main>
        )
    }
}


function mapStateToProps(state, props) {
    return {
        crypto: state.cryptoReducer.cryptos.filter( crypto => crypto.id === props.match.params.id )['0'],
        historical: state.cryptoReducer.historical
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getCryptos: (cmids) => dispatch(getCryptos(cmids)),
        getHistoricalCrypto: (cmid, period) => dispatch(getHistoricalCrypto(cmid, period))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CryptoPage)