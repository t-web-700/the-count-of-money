import React, { Component } from 'react'
import Chart from 'chart.js';

class CryptoChart extends Component {

    chart = null

    /**
     * Parent config to pass example
     * Props : id, height, width, config
     * when config is an object as
     * {
     *   title: '',
     *   backgroundColor: '',
     *   borderColor: '',
     *   data: [],
     *   labels: []
     * }
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            config: {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [
                        {
                            borderColor: '',
                            backgroundColor: '',
                            data: [],
                            fill: true,
                            lineTension: 0
                        }
                    ]
                },
                options: {
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: true,
                        text: ''
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: false,
                            scaleLabel: {
                                display: true,
                                labelString: ''
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                    }
                }
            }
        }
    }

    render(){
        return (
            <div>
                <canvas id={this.props.id} height={this.props.height} width={this.props.width} />
            </div>
        )
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if(prevProps.config.title !== this.props.config.title ||
            prevProps.config.backgroundColor !== this.props.config.backgroundColor ||
            prevProps.config.data !== this.props.config.data ||
            prevProps.config.labels !== this.props.config.labels
        ){
            this.setState({
                    ...prevState,
                    config: {
                        ...prevState.config,
                        data: {
                            ...prevState.config.data,
                            labels: this.props.config.labels,
                            datasets: [
                                {
                                    ...prevState.config.data.datasets[0],
                                    borderColor: this.props.config.borderColor,
                                    backgroundColor: this.props.config.backgroundColor,
                                    data: this.props.config.data,
                                }
                            ]
                        },
                        options: {
                            ...prevState.config.options,
                            title: {
                                ...prevState.config.options.title,
                                text: this.props.config.title
                            }
                        }
                    }
                },
                () => {
                    this.chart.options = this.state.config.options
                    this.chart.data.labels = this.state.config.data.labels
                    this.chart.data.datasets[0] = this.state.config.data.datasets[0]
                    this.chart.update()
                })
        }
    }

    componentDidMount() {
        this.chart = new Chart(this.props.id, this.state.config)
    }
}

export default CryptoChart