import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router-dom";
import {getArticle, getArticles} from "../../store/actions";

class NewsPage extends Component {
	constructor(props) {
		super(props);
		this.props.getArticle(this.props.match.params.id).then(
			success => {
				this.props.getArticles('', this.props.article.categories.join(','))
			}
		)
	}
	render(){
		const article = this.props.article
		const date = new Date(article.date)
		const tags = article.categories
		return (
			<main id="news">
				<header className="spaced shrinked bg-gradient white">
					<br/> {/*fix temp*/}
					<span className="news-date">{date.getDate()}/{date.getMonth() + 1}/{date.getFullYear()} à {date.getHours()}:{date.getMinutes()}</span>
					<h1>{article.title}</h1>
					<div className="news-tags">
						{
							(Array.isArray(tags)) ?
								tags.map((r,t) => (
									<button className="btn">{r}</button>
								)) : ''
						}
					</div>
				</header>
				<section id="news-page" className="spaced shrinked flex justify-between wrap">
					<div id="news-article" dangerouslySetInnerHTML={{__html: article.content}}>
					</div>
					<nav id="news-nav">
						<h2>Découvrir des articles similaires</h2>
						{
							(this.props.articles) ?
								(
									this.props.articles.slice(0, 6).map((r,t) => {
										if(r.title !== article.title){
											const date = new Date(r.date)
											const description = r.description.substring(0, 150) + " (...)";
											return (
												<Link to={'/news/' + r._id} className="flex-col">
													<span>{date.getDate()}/{date.getMonth() + 1}/{date.getFullYear()} à {date.getHours()}:{date.getMinutes()}</span>
													<h3>{r.title}</h3>
													<p>{description}</p>
												</Link>
											)
										}
									})) : ''
						}

					</nav>
				</section>
			</main>
		)
	}
}

function mapStateToProps(state) {
	return {
		article: state.articleReducer.article,
		articles: state.articleReducer.articles
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getArticle: (id) => dispatch(getArticle(id)),
		getArticles: (excludedTags, includedTags) => dispatch(getArticles(excludedTags, includedTags))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage)

// {this.props.match.params.test}
