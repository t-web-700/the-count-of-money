import React, { Component } from 'react'
import {connect} from 'react-redux'
import { action1, action2 } from '../../store/actions'

class App extends Component {
    render(){
        return (
            <div>
                <h1>Home</h1>
                <h2>{this.props.test}</h2>
                <button onClick={() => this.props.action1()}>action1</button>
                <button onClick={() => this.props.action2()}>action2</button>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        test: state.testReducer.test
    }
}

const mapDispatchToProps = {
    action1,
    action2
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
