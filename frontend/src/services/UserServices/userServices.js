import axios from 'axios'

const userServices = {
    getProfile,
    updateProfile
}

function getProfile(){
    return new Promise(((resolve, reject) => {
        axios.get(process.env.REACT_APP_API_URL + '/api/users/profile')
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

function updateProfile(username, email, currency, cryptos, keywords){
    return new Promise(((resolve, reject) => {
        axios.put(process.env.REACT_APP_API_URL + '/api/users/profile',
            {
                username: username,
                email: email,
                currency: currency,
                cryptos: cryptos,
                keywords: keywords
            })
            .then((response) => resolve(response.data))
            .catch((error) => reject('Une erreur est survenu lors la mise à jour de votre profil'))
    }))
}

export default userServices