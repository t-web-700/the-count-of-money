import axios from 'axios'

const articleServices = {
    getArticle,
    getArticles
}

function getArticles(excludedTags, includedTags){
    return new Promise(((resolve, reject) => {
        //cmid sparate with comma
        axios.get(process.env.REACT_APP_API_URL + '/api/articles?excludedTags=' + excludedTags + '&includedTags=' + includedTags,
            {})
            .then((response) => resolve(response.data))
            .catch((error) => reject('Une erreur est survenu lors du chargement des articles.'))
    }))
}

function getArticle(id){
    return new Promise(((resolve, reject) => {
        //cmid sparate with comma
        axios.get(process.env.REACT_APP_API_URL + '/api/articles/' + id)
            .then((response) => resolve(response.data))
            .catch((error) => reject("Une erreur est survenu lors de chargement de l'article"))
    }))
}

export default articleServices