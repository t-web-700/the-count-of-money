import axios from 'axios'

const authServices = {
    register,
    login,
    logout,
    authProvider
}

function register(username, email, password, repassword){
    return new Promise(((resolve, reject) => {
        if(password !== repassword){
            reject('Les mots de passe sont différents')
        }else {
            axios.post(process.env.REACT_APP_API_URL + '/api/users/register', {
                username: username,
                email: email,
                password: password
            })
                .then((response) => {
                    localStorage.setItem('user', JSON.stringify(response.data))
                    axios.defaults.headers.common['Authorization'] = "Bearer "+  response.data.token;
                    resolve(response.data)
                })
                .catch((error) => reject('Un problème est survenu lors de l\'enregistrement de votre compte. Veuillez réessayer ...'))
        }
    }))
}

function login(email, password){
    return new Promise(((resolve, reject) => {
        axios.post(process.env.REACT_APP_API_URL + '/api/users/login', {
            email: email,
            password: password
        })
            .then((response) => {
                localStorage.setItem('user', JSON.stringify(response.data))
                axios.defaults.headers.common['Authorization'] = "Bearer "+  response.data.token;
                resolve(response.data)
            })
            .catch((error) => reject('Identifiant ou mot de passe incorrect ! Veuillez réessayer ...'))
    }))
}

function logout(){
    return new Promise(((resolve, reject) => {
        axios.post(process.env.REACT_APP_API_URL + '/api/users/logout')
            .then((response) => resolve(response.data))
            .catch((error) => reject('Une erreur est survenu lors de la déconnexion ! Veuillez réessayer ...'))
    }))
}

function authProvider(provider){
    return new Promise(((resolve, reject) => {
        axios.get(process.env.REACT_APP_API_URL + '/api/users/auth/' + provider)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

export default authServices