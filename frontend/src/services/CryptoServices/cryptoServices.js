import axios from 'axios'

const cryptoServices = {
    getCrypto,
    getCryptos,
    getHistoricalCrypto,
    sendCrypto,
    deleteCrypto
}

function getCryptos(cmids){
    return new Promise(((resolve, reject) => {
        let newCmids = cmids.join()
        axios.get(process.env.REACT_APP_API_URL + '/api/cryptos?cmids=' + newCmids)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

function getCrypto(cmid){
    return new Promise(((resolve, reject) => {
        axios.get(process.env.REACT_APP_API_URL + '/api/cryptos/' + cmid)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

function getHistoricalCrypto(cmid, period){
    return new Promise(((resolve, reject) => {
        //cmid sparate with comma
        axios.get(process.env.REACT_APP_API_URL + '/api/cryptos/' + cmid + '/history/' + period)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

function sendCrypto(code, name, img, cmid){
    return new Promise(((resolve, reject) => {
        //cmid sparate with comma
        axios.post(process.env.REACT_APP_API_URL + '/api/cryptos',
            {
                code: code,
                name: name,
                img: img,
                id: cmid
            })
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

function deleteCrypto(cmid){
    return new Promise(((resolve, reject) => {
        axios.delete(process.env.REACT_APP_API_URL + '/api/cryptos/' + cmid)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    }))
}

export default cryptoServices
