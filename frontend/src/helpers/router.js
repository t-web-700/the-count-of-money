import {Route} from "react-router-dom"
import React from "react"
import HomePage from "../components/Home/HomePage"
import NewsPage from "../components/News/NewsPage"
import ProfilPage from "../components/Profile/ProfilePage"
import AuthPage from "../components/Auth/AuthPage"
import Error404 from "../components/Error/Error404"
import LoginPart from "../components/Auth/LoginPart"
import RegisterPart from "../components/Auth/RegisterPart"
import history from "../history"
import userServices from "../services/UserServices/userServices";
import CryptoPage from "../components/Crypto/CryptoPage";
import {Redirect} from "react-router";
import LegalePage from "../components/Legale/LegalePage";

const router = {
    getRoutes,
    Routes
}

const routes = [
    {
        name: 'Home',
        routes: [
            {
                path: '/home',
                component: HomePage,
                exact: true,
                auth: false
            }
        ]
    },
    {
        name: 'Auth',
        routes: [
            {
                //login - register
                path: '/auth/:type',
                component: AuthPage,
                exact: true,
                auth: false,
                subroutes: [
                    {
                        path: '/auth/login',
                        component: LoginPart,
                        exact: true,
                        auth: false
                    },
                    {
                        path: '/auth/register',
                        component: RegisterPart,
                        exact: true,
                        auth: false
                    }
                ]
            },
            {
                path: '/auth',
                component: AuthPage,
                exact: true,
                auth: false
            }
        ]
    },
    {
        name:'News',
        routes: [
            {
                path: '/news/:id',
                component: NewsPage,
                exact: true,
                auth: false
            },
            {
                path: '/news',
                component: NewsPage,
                exact: true,
                auth: false
            }
        ]
    },
    {
        name: 'Profile',
        routes: [
            {
                path: '/profile',
                component: ProfilPage,
                exact: true,
                auth: true
            }
        ]
    },
    {
        name: 'Crypto',
        routes: [
            {
                path: '/crypto/:id',
                component: CryptoPage,
                exact: true,
                auth: true
            }
        ]
    },
    {
        name: 'Legal',
        routes: [
            {
                path: '/legal',
                component: LegalePage,
                exact: true,
                auth: false
            }
        ]
    },
    {
        name: 'Root',
        routes: [
            {
                path: '/',
                component: HomePage,
                exact: true,
                auth: false
            }
        ]
    },
    {
        name: 'All',
        routes: [
            {
                path: '*',
                component: Error404,
                auth: false
            }
        ]
    }
]

function getRoutes(store){
    return routes
}

function Routes(route) {
    if(!localStorage.getItem('user') && route.auth === true) {
            history.push('/home')
    }else if(localStorage.getItem('user')){
        userServices.getProfile()
        if(route.path === '/auth/:type' || route.path === '/auth'){
            history.push('/home')
        }
    }
    return (
        <Route
            exact={route.exact}
            path={route.path}
            render={
                props => (<route.component {...props} routes={route.subroutes}/>)
            }
        />)
}

export default router