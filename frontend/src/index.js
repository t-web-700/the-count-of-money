import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Switch, Router } from "react-router-dom";
import router from './helpers/router'
import {Provider} from "react-redux"
import {createStore, applyMiddleware} from "redux"
import rootReducer from './store/reducers'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import {loginYet, logout} from './store/actions'
import axios from 'axios'
import thunk from 'redux-thunk'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import history from "./history"

toast.configure({
    position: toast.POSITION.BOTTOM_RIGHT
})

const store = createStore(rootReducer, applyMiddleware(thunk))

const {dispatch} = store;

axios.interceptors.response.use(
    response => response,
    error => {
        const {status} = error.response;
        if ((status === 401 || status === 403) && axios.defaults.headers.common['Authorization'] ) {
            dispatch(logout());
        }
        return Promise.reject(error);
    }
)

if(localStorage.getItem('user')){
    axios.defaults.headers.common['Authorization'] = "Bearer " +  JSON.parse(localStorage.getItem('user')).token
    store.dispatch(loginYet())
}

class Root extends Component {
    render(){
        return (
            <Provider store={store}>
                <Router history={history}>
                    <Header></Header>
                    <div>
                        <Switch>
                            {router.getRoutes().map((route, i) => {
                                return (
                                    route.routes.map((r, j) => (
                                            <router.Routes key={j} {...r} store={store}/>
                                        )
                                    )
                                )})}
                        </Switch>
                    </div>
                    <Footer></Footer>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));

serviceWorker.register();