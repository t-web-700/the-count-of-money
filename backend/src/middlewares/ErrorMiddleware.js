const httpStatus = require('http-status-codes')

const config = require('../config')
const TokenError = require('../errors/TokenError')
const InvalidHeadersError = require('../errors/InvalidHeadersError')
const InvalidArgumentError = require('../errors/InvalidArgumentError')
const UnauthorizedUserError = require('../errors/UnauthorizedUserError')

module.exports = function(err, req, res, next) {
  // Anyway we log the error
  console.error(err)
  // default value for the response
  let status = 500
  let message = null

  // handle specifics errors
  if (err instanceof InvalidArgumentError) {
    status = 400
    message = err.message
  }

  if (err instanceof TokenError || err instanceof InvalidHeadersError || err instanceof UnauthorizedUserError) {
    status = httpStatus.FORBIDDEN
    message = { error: err.message }
  }

  // create the response

  if (config.node_env == 'development') {
    // we ignore the user specific message and return the error instead
    res.status(status).json({ "err": `${err}`, 'stack': `${err.stack}`, ...err})
  } else {
    res.status(status).send(message)
  }
}