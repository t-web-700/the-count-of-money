const AuthHelper = require('../helpers/AuthHelper')
const UserService = require("../services/user/UserService")
const InvalidHeadersError = require("../errors/InvalidHeadersError")
const UnauthorizedUserError = require("../errors/UnauthorizedUserError")


async function authMiddleware(req) {
  if (!req.headers.authorization || req.headers.authorization.split(' ')[0] !== "Bearer" && req.headers.authorization.split(' ').length !== 2)
    throw new InvalidHeadersError("The authorization header is not correct")
  try {
    const token = req.headers.authorization.split(' ')[1]
    const { sub } = await AuthHelper.verifyJWT(token)
    const user = await UserService.getUserById(sub)
    return user
  } catch (err) {
    console.error(err.message)
    throw err
  }
}

async function userMiddleware(req, res, next) {
  try {
    const user = await authMiddleware(req)
    res.locals.user = user
    return next()
  } catch (err) {
    console.error(err.message)
    return next(err)
  }
}

async function adminMiddleware(req, res, next) {
  try {
    const user = await authMiddleware(req)
    if (user.role !== "ADMIN")
      throw new UnauthorizedUserError("Your role does not allow you to access this resource")
    res.locals.user = user
    return next()
  } catch (err) {
    console.error(err.message)
    return next(err)
  }
}

async function injectUserMiddleware(req, res, next) {
  try {
    const user = await authMiddleware(req)
    res.locals.user = user
    return next()
  } catch (err) {
    console.error(err.message)
    return next()
  }
}

module.exports = {
  userMiddleware,
  adminMiddleware,
  injectUserMiddleware
}
