class InvalidHeadersError extends Error {
  constructor(message) {
    super(message);
    this.name = "InvalidHeadersError";
  }
}

module.exports = InvalidHeadersError
