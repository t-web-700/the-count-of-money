class CryptoAlreadyExistsError extends Error {
  constructor(message) {
    super(message);
    this.name = "CryptoAlreadyExistsError";
  }
}

module.exports = CryptoAlreadyExistsError
