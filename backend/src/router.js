const express = require('express')

const ErrorMiddleware = require('./middlewares/ErrorMiddleware')

const UserController = require('./services/user/UserController')
const CryptoController = require('./services/crypto/CryptoController')
const ArticleController = require('./services/article/ArticleController')
const PreferenceController = require('./services/preference/PreferenceController')

const router = express.Router()


router.use(express.json())
router.use('/users', UserController)
router.use('/cryptos', CryptoController)
router.use('/articles', ArticleController)
router.use('/preferences', PreferenceController)

router.use(ErrorMiddleware)

module.exports = router
