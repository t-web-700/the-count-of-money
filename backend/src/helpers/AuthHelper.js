const jwt = require('jsonwebtoken')

const config = require('../config')
const TokenError = require("../errors/TokenError")


const jwtOptions = {
  expiresIn: '1d',
  algorithm: 'RS256',
  issuer: 'batman',
}

function generateJWT(aud, sub, payload) {
  const options = { ...jwtOptions }
  
  options.audience = aud
  options.subject = sub
  try {
    return jwt.sign(payload, config.authPrivateKey, options)
  } catch (error) {
    throw new TokenError('Cannot sign the json web token')
  }
}

function verifyJWT(token) {
  try {
    return jwt.verify(token, config.authPublicKey, { audience: 'robin', algorithms: ['RS256'] })
  } catch (error) {
    throw new TokenError('Cannot verify the json web token')
  }
}

module.exports = {
  generateJWT,
  verifyJWT
}
