const mongoose = require('mongoose');

const config = require('./config');
const User = require('./services/user/UserModel')
const UserSerice = require('./services/user/UserService')
const CryptoService = require('./services/crypto/CryptoService')
const ArticleService = require('./services/article/ArticleService')
const Preference = require('./services/preference/PreferenceModel')


async function connectDatabase() {
  const uri = `mongodb://${config.db_host}:${config.db_port}`
  const options = {
    useCreateIndex: true, 
    useUnifiedTopology: true, 
    useNewUrlParser: true,
    user: config.db_user,
    pass: config.db_pass
  }

  try {
    await mongoose.connect(uri, options)
  } catch (error) {
    console.error("Error connect to DB:", error)
    throw error
  }
  mongoose.connection
    .on('error', (err) => console.error('Connection error:', err.message))
    .once('open', () => console.log('Connected to DB!'))
}

async function initializePreferences() {
  try {
    await new Preference(config.preferences).save()
    await ArticleService.readSavedFeedsAndStoreArticles()
    console.log("Initializing preferences")
  } catch (error) {
    console.error("Error initializing preferences:", error.message)
  }
}

async function initializeCryptos() {
  const btc = {
    code: "btc",
    name: "Bitcoin",
    imageUrl: "https://static.coinpaprika.com/coin/btc-bitcoin/logo-thumb.png",
    paprikaId: "btc-bitcoin"
  }
  try {
    await CryptoService.registerCrypto(btc)
    console.log("Initializing cryptos")
  } catch (error) {
    console.error("Error initializing cryptos:", error.message)
  }
}

async function initializeUsers() {
  try {
    const admin = await UserSerice.register({ username: "Admin Admin", email: "admin@localhost", password: "admin" })
    const user = await UserSerice.register({ username: "User User", email: "user@localhost", password: "user" })
    
    await User.findOneAndUpdate({ _id: admin._id }, { role: "ADMIN" }, { upsert: true, useFindAndModify: false })
    await User.findOneAndUpdate({ _id: user._id }, { role: "USER" }, { upsert: true, useFindAndModify: false })
    console.log("Initializing users")
  } catch (error) {
    console.error("Error initializing users:", error.message)
  }
}

async function initialize() {
  await connectDatabase()
  await initializePreferences()
  await initializeCryptos()
  await initializeUsers()
}

module.exports = {
  initialize
}
