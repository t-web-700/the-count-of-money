const mongoose = require('mongoose');

Article = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    creator: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    sourceRss: {
        type: String,
        required: true
    },
    categories: {
        type: [String],
        required: true
    },
    articleLink: {
        type: String,
        required: true,
        unique: true
    },
    sourceWebsiteTitle: {
        type: String,
        required: true
    },
    sourceWebsiteLink: {
        type: String,
        required: true
    },
    sourceWebsiteDescription: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Article', Article);
