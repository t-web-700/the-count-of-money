const express = require('express')
const router = express.Router()
const httpStatus = require('http-status-codes')

const PressService = require('./ArticleService')
const Auth = require("../../middlewares/AuthMiddleware")


/**
 * @api {get} /articles/process Process the articles and save the content in the db
 * @apiName processFeeds
 * @apiGroup Articles
 *
 * @apiSuccess {Article[]} the created articles by feed 
 */
router.get('/process', Auth.adminMiddleware, async (req, res, next) => {
  try {
    res.json(await PressService.readSavedFeedsAndStoreArticles())   
  } catch (error) {
    next(error)
  }
})

/**
 * @api {delete} /articles/ Delete all articles stored in DB
 * @apiName deleteAllArticles
 * @apiGroup Articles
 */
router.delete('/', Auth.adminMiddleware, async (req, res, next) => {
  try {
    await PressService.deleteAllArticles()
    res.status(httpStatus.OK).send('ok')
  } catch (error) {
    next(error)
  }
})

/**
 * @api {get} /articles/:id Get an article for an id
 * @apiName getArticleForId
 * @apiGroup Articles
 *
 * @apiSuccess {Article} the article 
 */
router.get('/:id', async (req, res, next) => {
  try {
    const id = req.params.id
    res.status(httpStatus.OK).json(await PressService.getArticleForId(id))
  } catch (error) {
    next(error)
  }
})

/**
 * @api {get} /articles/ Get All articles can include or exclude some tags
 * @apiName getArticles
 * @apiGroup Articles
 *
 * @apiSuccess {Article[]} The list of articles
 */
router.get('/', Auth.injectUserMiddleware, async (req, res, next) => {
  try {
    let includedTags = []
    let excludedTags = []
    if (res.locals.user) {
      excludedTags = req.query.excludedTags ? req.query.excludedTags.split(',') : []
      includedTags = req.query.includedTags ? req.query.includedTags.split(',') : []
    }
    res.json(await PressService.getArticlesMatchingTags(excludedTags, includedTags))   
  } catch (error) {
    next(error)
  }
})

module.exports = router
