const RssParser = require('rss-parser');

const PreferenceService = require('../preference/PreferenceService')
const Article = require('./ArticleModel')


/**
 * Process the feeds and save the articles in the database
 * @returns {Articles[]} created articles by feeds
 */
async function readSavedFeedsAndStoreArticles() {
    const feeds = (await PreferenceService.getPreferences()).feeds
    return Promise.all(feeds.map(async feed => await readFeedAndStoreArticleInDB(feed)))
}

/**
 * Process one feed and save the articles in the database
 * @param {string} rssFeed the rss feed
 * @returns {Articles[]} the created articles for the feed
 */
async function readFeedAndStoreArticleInDB(rssFeed) {
    const parser = new RssParser()
    let feed = await parser.parseURL(rssFeed)

    // Get the website informations
    const sourceWebsite = {
        title: feed.title,
        link: feed.feedUrl,
        description: feed.description
    }

    // Convert the feed data to a database compatible format
    const toSave = feed.items.map(item => {
        // Get the content data
        let content = item["content:encoded"] ? item["content:encoded"] : item["content"]

        // Replace the \n by </br>
        content = content.replace('\\n', '</br>')
        const snipet = item.contentSnippet.replace('\\n', '</br>')
        const categories = item.categories.map(cat => cat.toLowerCase())

        return {
            title: item.title,
            description: snipet,
            creator: item.creator,
            content: content,
            date: Date.parse(item.isoDate),
            sourceRss: rssFeed,
            categories: categories,
            articleLink: item.link,
            sourceWebsiteTitle: sourceWebsite.title,
            sourceWebsiteLink: sourceWebsite.link,
            sourceWebsiteDescription: sourceWebsite.description
        }
    })
    
    // Save the data and get the saved objects
    let savedArticles = []
    for (article of toSave) {
        try {
            const savedArticle = await new Article(article).save()
            savedArticles.push(savedArticle)
        } catch (e) {
            if (false == e.message.includes('duplicate key error collection')) {
                console.error('Warning cannot save article', e)
            }
        }
    }
    return savedArticles
}

/**
 * Delete all articles in the database
 */
async function deleteAllArticles() {
    await Article.deleteMany({})
}

/**
 * Get an article for an id
 * @param {string} id the article id
 * @returns {Article} the article
 */
async function getArticleForId(id) {
    return await Article.findOne({'_id': id})
}

/**
 * Get all Saved Articles
 * @returns {Article} the saved articles
 */
async function getAllArticles() {
    return await Article.find({})
}

/**
 * Get all articles excluding or including some tags
 * @param {string[]} excludedTags the list of excludedTags
 * @param {string[]} includedTags the list of includedTags
 * 
 * @returns {Article[]} the list of articles
 */
async function getArticlesMatchingTags(excludedTags = [], includedTags = []) {
    let articles = await getAllArticles()
    articles = articles.filter(article => {
        return !excludedTags.some(tag => article.categories.includes(tag))
    })
    if (includedTags.length > 0) {
        articles = articles.filter(article => {
            return includedTags.some(tag => article.categories.includes(tag))
        })
    }
    return articles
}

module.exports = {
    readSavedFeedsAndStoreArticles,
    readFeedAndStoreArticleInDB,
    deleteAllArticles,
    getArticleForId,
    getAllArticles,
    getArticlesMatchingTags
}
