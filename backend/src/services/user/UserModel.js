const mongoose = require('mongoose');


User = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["ADMIN", "USER"],
        required: true,
        default: "USER"
    },
    currency: {
        type: String,
        enum: ["EUR", "USD"],
        required: true,
        default: "EUR"
    },
    cryptos: {
        type: [String],
        required: true
    },
    keywords: {
        type: [String]
    }
});

User.query.byUserId = function(id) {
    return this.where({ _id: id });
}

module.exports = mongoose.model('User', User);
