const bcrypt = require('bcrypt')

const User = require("./UserModel")
const config = require('../../config')
const AuthHelper = require('../../helpers/AuthHelper')
const CryptoService = require("../crypto/CryptoService")
const InvalidArgumentError = require('../../errors/InvalidArgumentError')
const UserAlreadyExistsError = require('../../errors/UserAlreadyExistsError')


/**
 * 
 * @param {username} username the username of user
 * @param {email} email the user's email 
 * @param {password} password the password set by the user 
 */
async function register({ username, email, password }) {
  if (!username || !email || !password)
    throw new InvalidArgumentError('Invalid arguments')

  try {
    const params = { username, email }
    params.password = bcrypt.hashSync(password + config.salt, config.saltRounds)
    params.cryptos = (await CryptoService.getAllCryptos()).map(crypto => crypto.id)
    params.keywords = []
    const user = await new User(params).save()
    return user
  } catch (error) {
    console.error(error.message)
    throw new UserAlreadyExistsError('User already exists')
  }
}

/**
 * 
 * @param {email} email email address of the user
 * @param {password} password password address of the user
 */
async function login({ email, password }) {
  try {
    const user = await User.findOne({ email: email })
    const match = bcrypt.compareSync(password + config.salt, user.password);
    if (!match) throw new Error('Invalid login credentials')
    const payload = { admin: user.role == "ADMIN" ? true : false, username: user.username }
    const jwt = AuthHelper.generateJWT('robin', String(user._id), payload)
    return { token: jwt }
  } catch (err) {
    console.error(err.message)
    throw new Error('Invalid login credentials')
  }
}

/**
 * 
 * @param {userId} userId the user's id
 */
async function getUserById(userId) {
  try {
    let user = await User.findById(userId).lean()
    delete user.password
    delete user.__v
    return user
  } catch (error) {
    console.error(error.message)
    throw error
  }
}

/**
 * 
 * @param {userId} userId The user's id 
 * @param {params} params The request body params 
 */
async function updateUser(userId, params) {
  if (Object.keys(params).length === 0) throw new InvalidArgumentError("Missing body request params")
  if (Object.keys(params).includes("password")) throw new InvalidArgumentError("You can't modify the password")

  try {
    let user = await User.findOneAndUpdate({ _id: userId }, params, { new: true, upsert: true, useFindAndModify: false }).lean()
    delete user.password
    delete user.__v
    return user
  } catch (error) {
    console.error(error.message)
    throw error
  }
}

module.exports = {
  register,
  login,
  getUserById,
  updateUser
}
