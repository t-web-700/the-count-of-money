const express = require("express")
const router = express.Router()
const httpStatus = require('http-status-codes')

const userService = require("./UserService")
const Auth = require("../../middlewares/AuthMiddleware")


/**
 * @api {post} /api/users/register Create one User
 * @apiName Register
 * @apiGroup Users
 *
 * @apiParam {String} username The User username
 * @apiParam {String} email The User email
 * @apiParam {String} password The User password
 */
router.post("/register", async (req, res, next) => {
  try {
    const user = await userService.register(req.body)
    const auth = await userService.login(req.body)

    console.log("Created user", user)
    console.log("Created token", auth)
    return res.status(httpStatus.CREATED).send(auth)
  } catch (error) {
    console.error(error.message)
    return res.status(httpStatus.BAD_REQUEST).send({error: error.message})
  }
})

/**
 * @api {post} /api/users/login Login User
 * @apiName Login
 * @apiGroup Users
 *
 * @apiParam {String} email The User email
 * @apiParam {String} password The User password
 */
router.post("/login", async (req, res, next) => {
  try {
    const auth = await userService.login(req.body)

    console.log("Created token", auth)
    return res.status(httpStatus.OK).send(auth)
  } catch (error) {
    console.error(error.message)
    return res.status(httpStatus.BAD_REQUEST).send({ error: error.message })
  }
})

/**
 * @api {post} /api/users/logout Logout User
 * @apiName Logout
 * @apiGroup Users
 *
 * @apiParam None
 */
router.post("/logout", async (req, res, next) => {
  return res.status(httpStatus.OK).send({ token: "" })
})

/**
 * @api {get} /api/users/profile Retrieve the user's profile
 * @apiName GetProfile
 * @apiGroup Users
 *
 * @apiParam None
 */
router.get("/profile", Auth.userMiddleware, async (req, res, next) => {
  try {
    const user = await userService.getUserById(res.locals.user._id)
    console.log("User found", user)
    return res.status(httpStatus.OK).send(user)
  } catch (error) {
    console.error(error)
    return res.status(httpStatus.FORBIDDEN).send({ error: error.message })
  }
})

/**
 * @api {put} /api/users/profile Update the user's profile
 * @apiName UpdateProfile
 * @apiGroup Users
 *
 * @apiParam {String} username The user's username
 * @apiParam {String} email The user's email
 * @apiParam {String} currency The default currency for the price of crypto-currencies
 * @apiParam {String} cryptos The list of cryto-currencies, to appear on their home page
 * @apiParam {String} keywords The list of keywords, for their press review
 */
router.put("/profile", Auth.userMiddleware, async (req, res, next) => {
  // TODO: validate body request, then if OK, process to update, then return the updated user 
  try {
    const result = await userService.updateUser(res.locals.user._id, req.body)
    console.log("User updated", result)
    return res.status(httpStatus.OK).send(result)        
  } catch (error) {
    console.error(error)
    return res.status(httpStatus.BAD_REQUEST).send({ error: error.message })
  }
})

module.exports = router;
