const CoinpaprikaAPI = require('@coinpaprika/api-nodejs-client')

const Crypto = require('./CryptoModel')
const InvalidArgumentError = require('../../errors/InvalidArgumentError')
const CryptoAlreadyExistsError = require('../../errors/CryptoAlreadyExistsError')


const paprika = new CoinpaprikaAPI()

const PERIODS_FOR_HISTORICAL = new Map([ // in seconds
  [ 'daily',  { 'dataDuration': 60 * 60 * 24 * 60, 'interval': '1d' }],
  [ 'hourly', { 'dataDuration': 60 * 60 * 48,      'interval': '1h' }],
  [ 'minute', { 'dataDuration': 60 * 60,           'interval': '5m' }]
])

let paprikaCachedCoinList = null;
async function getPaprikaCachedCoinList() {
  if (paprikaCachedCoinList === null) {
    paprikaCachedCoinList = await paprika.getCoins()
  }
  return paprikaCachedCoinList
}

/**
 * Get the historicalData for a cryptocurrency
 * @param {string} cmid the cryptocurrency id
 * @param {string} period the cryptocurrency id 
 * @returns {CoinHistoricalData} the Coin historical data
 */
async function getHistoricalData({ cmid, period }) {

  const crypto = await getCryptoById(cmid)

  const currency = "usd"

  // check arguments
  if (!cmid || Array.from(PERIODS_FOR_HISTORICAL.keys()).includes(period) == false) {
    throw new InvalidArgumentError('Invalid arguments for getHistoricalData')
  }

  const now = new Date()
  const startTime = new Date(now.getTime() - (PERIODS_FOR_HISTORICAL.get(period).dataDuration * 1000))

  // Get Raw Data from paprika and map the atributes to the project api

  const dailyData = (await paprika.getCoinsOHLCVHistorical({
    coinId: cmid,
    quote: currency,
    start: startTime.toISOString(),
    end: now.toISOString()
  })).map(dailyDataItem => { 
    return {
      date: dailyDataItem.time_open,
      highest: dailyDataItem.high,
      lowest: dailyDataItem.low,
      opening: dailyDataItem.open,
      closing: dailyDataItem.close
    }
  })

  let intervalData = (await paprika.getAllTickers({
    coinId: cmid,
    quote: currency,
    historical: {
      start: startTime.toISOString(),
      end: now.toISOString(),
      interval: PERIODS_FOR_HISTORICAL.get(period).interval
    }}))
    .map(intervalDadaItem => {
      return {
        date: intervalDadaItem.timestamp,
        price: intervalDadaItem.price
      }
  })

  if (period == 'minute') {
    let generatedIntervalData = []
    let prev = null
    intervalData.forEach(next => {
      if (prev == null) {
        prev = next
        generatedIntervalData.push(next)
      } else {
        const coef = (next.price - prev.price) / 5
        for (let i = 1; i < 5; i++) {
          prev = {
            date: new Date(new Date(prev.date).getTime() + 60 * 1000).toISOString(),
            price: Math.round((prev.price + coef) * 100) / 100
          }
          generatedIntervalData.push(prev)
        }
        prev = next
        generatedIntervalData.push(next)
      }
    })
    intervalData = generatedIntervalData
  }

  const lowest = intervalData.reduce((min, actual) => (min.price < actual.price) ? min : actual).price
  const highest = intervalData.reduce((max, actual) => (max.price > actual.price) ? max : actual).price

  return {
    start: startTime.toISOString(),
    end: now.toISOString(),
    interval: period,
    currency,
    highest,
    lowest,
    dailyData,
    intervalData
  }
}

/**
 * Get the informations for a set of cryptocurencies
 * @param {string[]} cmids the ids of the cryptocurrency
 * @returns {CryptoInfos[]} the infos for the cryptos
 */
async function getCryptosInfos(cmids = []) {
  const currency = "usd"
  const now = new Date()

  let promises = cmids.map(async (cmid) => {
    const crypto = await getCryptoById(cmid)

    const todayDailyItem = (await paprika.getCoinsOHLCVHistorical({
      coinId: cmid,
      quote: currency,
      start: now.toISOString()
    })).map(dailyDataItem => { 
      return {
        highest: dailyDataItem.high,
        lowest: dailyDataItem.low,
        opening: dailyDataItem.open
    }})[0] // get first item of the array

    const latestPriceObject = (await paprika.getAllTickers({
      coinId: cmid,
      historical: {
        start: now.toISOString().split('T')[0]
      }}))
      .map(intervalDadaItem => {
        return {
          price: intervalDadaItem.price
        }
      }).slice(-1).pop() // Get the last item of an array

      const cryptoName = (await getPaprikaCachedCoinList())
        .filter(coin => coin.id == cmid).map(coin => coin.name)[0]
      
      return {
        id: crypto.paprikaId,
        name: crypto.name,
        code: crypto.code,
        price: latestPriceObject.price,
        highest: todayDailyItem.highest,
        lowest: todayDailyItem.lowest,
        opening: todayDailyItem.opening,
        image_url: crypto.imageUrl
      }
  })
  let result = []
  for (const promise of promises) {
    try {
      let res = await promise
      result.push(res)
    } catch (error) {
      console.error(error.message)
    }
  }
  return result
}

/**
 * Register a crypto for the application
 * @param {*} params the cryptocurency to register 
 */
async function registerCrypto(params = {}) {
  if (!params.code || !params.name || !params.imageUrl || !params.paprikaId) {
    throw new InvalidArgumentError('Invalid arguments')
  }
  try {
    const crypto = await new Crypto(params).save()
    return crypto
  } catch (error) {
    console.error(error.message)
    throw new CryptoAlreadyExistsError('Crypto already exists')
  }
}

/**
 * Get the infos for a cryptocurency matching the id 
 * @param {paprikaId} paprikaId the id of the crypto
 * @returns {CryptoInfos} the infos for the crypto
 */
async function getCryptoById(paprikaId) {
  if (!paprikaId) {
    throw new InvalidArgumentError(`"${paprikaId}" is not a valid Id`)
  }
  const crypto = await Crypto.findOne().byPaprikaId(paprikaId).exec()
  if (!crypto) {
    throw new InvalidArgumentError(`"${paprikaId}" is not registered`)
  }
  return crypto
}

/**
 * Get all the crypocurrency infos
 * @returns {CryptoInfos[]} the infos for the cryptos
 */
async function getAllCryptos() {
  try {
    const crypto = await Crypto.find({})
    const cryptoIds = crypto.map(crypto => crypto.paprikaId)
    return await getCryptosInfos(cryptoIds)
  } catch (error) {
    console.error(error.message)
    throw error
  }
}

/**
 *  Delete a crypto from the application
 * @param {paprikaId} paprikaId the id of the crypto
 */
async function deleteCrypto(paprikaId) {
  Crypto.findOneAndDelete().byPaprikaId(paprikaId).exec()
}

module.exports = {
  getHistoricalData,
  getCryptosInfos,
  registerCrypto,
  getCryptoById,
  getAllCryptos,
  deleteCrypto
}
