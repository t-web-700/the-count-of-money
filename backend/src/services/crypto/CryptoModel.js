const mongoose = require('mongoose');

Crypto = new mongoose.Schema({
    code: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    paprikaId: {
        type: String,
        unique: true,
        required: true
    }
});

Crypto.query.byPaprikaId = function(id) {
    return this.where({ paprikaId: id });
}

module.exports = mongoose.model('Crypto', Crypto);
