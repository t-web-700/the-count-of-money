const express = require('express')
const router = express.Router()
const httpStatus = require('http-status-codes')

const Auth = require("../../middlewares/AuthMiddleware")
const CryptocurrencyService = require('./CryptoService')


/**
 * @api {get} /cryptos/:cmid/history/:period Get the coin historical data for a period
 * @apiName getCoinHistoricalData
 * @apiGroup Cryptos
 *
 * @apiSuccess {CoinHistoricalData} the Coin historical data
 */
router.get('/:cmid/history/:period', Auth.userMiddleware, async (req, res, next) => {
  try {
    const historicalData = await CryptocurrencyService.getHistoricalData({
      cmid: req.params.cmid,
      period: req.params.period
    })
    res.json(historicalData)   
  } catch (error) {
    next(error)
  }
})

/**
 * @api {post} /cryptos/ Register a crypto for the application
 * @apiName registerCrypto
 * @apiGroup Cryptos
 *
 * @apiSuccess {CryptoModel} the crypto as saved in DB
 */
router.post('/', Auth.adminMiddleware, async (req, res, next) => {
  try {
    res.json(await CryptocurrencyService.registerCrypto(req.body))
  } catch (error) {
    next(error)
  }
})

/**
 * @api {get} /cryptos/:id Get the infos for a cryptocurency matching the id 
 * @apiName getCryptoInfos
 * @apiGroup Cryptos
 *
 * @apiSuccess {CryptoInfos} the infos for the crypto
 */
router.get('/:cmid', Auth.userMiddleware, async (req, res, next) => {
  try {
    res.json((await CryptocurrencyService.getCryptosInfos([req.params.cmid]))[0])
  } catch (error) {
    next(error)
  }
})

/**
 * @api {delete} /cryptos/:id Delete a crypto from the application
 * @apiName deleteCrypto
 * @apiGroup Cryptos
 */
router.delete('/:cmid', Auth.adminMiddleware, async (req, res, next) => {
  try {
    await CryptocurrencyService.deleteCrypto([req.params.cmid])
    res.status(httpStatus.OK).send({ status: "OK" })
  } catch (error) {
    next(error)
  }
})

/**
 * @api {get} /cryptos/ Get the informations for a set of cryptocurencies
 * @apiName getAllCryptos
 * @apiGroup Cryptos
 *
 * @apiSuccess {CryptoInfos[]} the infos for the cryptos
 */
router.get('/', async (req, res, next) => {
  let cryptos
  try {
    if (!req.query.cmids) {
      cryptos = await CryptocurrencyService.getAllCryptos()
    } else {
      const cmids = req.query.cmids.split(',')
      cryptos = await CryptocurrencyService.getCryptosInfos(cmids)
    }
    return res.status(httpStatus.OK).send(cryptos)
  } catch (error) {
    next(error)
  }
})

module.exports = router
