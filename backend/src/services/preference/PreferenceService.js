const Preferences = require('./PreferenceModel')


async function getPreferences() {
  try {
    let pref = await Preferences.findOne({}).lean()
    delete pref._id
    delete pref.__v
    console.log("Preference found", pref)
    return pref
  } catch (error) {
    console.error(error)
    throw error
  }
}

async function savePreferences(newPrefs) {
  try {
    let pref = await Preferences.findOneAndUpdate({}, newPrefs, { new: true, upsert: true, useFindAndModify: false }).lean()
    delete pref._id
    delete pref.__v
    console.log("Preference found", pref)
    return pref
  } catch (error) {
    console.error(error)
    throw error
  }
}

module.exports = {
  getPreferences,
  savePreferences
}
