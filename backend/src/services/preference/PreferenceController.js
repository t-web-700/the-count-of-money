const express = require('express')
const router = express.Router()
const httpStatus = require('http-status-codes')

const Auth = require("../../middlewares/AuthMiddleware")
const PreferenceService = require('./PreferenceService')


/**
 * @api {get} /api/preferences Retrieve app preferences
 * @apiName GetPreferences
 * @apiGroup Preferences
 *
 * @apiParam None
 */
router.get('/', async (req, res, next) => {
  try {
    const prefs = await PreferenceService.getPreferences()
    res.status(httpStatus.OK).send(prefs)
  } catch (error) {
    next(error)
  }
})

/**
 * @api {put} /api/preferences Update app preferences
 * @apiName UpdatePreferences
 * @apiGroup Preferences
 *
 * @apiParam {String[]} feeds List of RSS feeds
 * @apiParam {String[]} cryptos List of cryptos available 
 */
router.put('/', Auth.adminMiddleware, async (req, res, next) => {
  try {
    let prefs = await PreferenceService.savePreferences(req.body)
    res.status(httpStatus.OK).send(prefs)
  } catch (error) {
    next(error)
  }
})

module.exports = router
