const mongoose = require('mongoose');

Preferences = new mongoose.Schema({
  feeds: {
    type: [String],
    required: true
  },
  cryptos: {
    type: [String],
    required: true
  }
})

module.exports = mongoose.model('Preferences', Preferences)
