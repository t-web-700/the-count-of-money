const dotenv =  require('dotenv');
dotenv.config();

const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');
const router = require('./router');
const database = require('./database');

const app = express();


function applyCORS() {
  const whitelist = ['http://localhost:3000'];
  const corsOptions = {
    origin(origin, callback) {
      if (whitelist.indexOf(origin) !== -1) callback(null, true);
      else callback(null, false);
    },
  };
  app.use(cors(corsOptions));
}

async function main() {

  try {
    await database.initialize();
    applyCORS();
    app.use(bodyParser.json({ limit: '100mb' }));
    app.use('/api', router);
    app.listen(config.port);
    console.log(`App listening on ${config.port}`);
  } catch (error) {
    console.error('App crash', error)
    process.exit(1);
  }

}

main();

module.exports = { app };
