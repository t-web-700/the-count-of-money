#!/bin/bash

export ENDPOINT=http://localhost:4000

# Register a crypto
http --json POST $ENDPOINT/api/cryptos <<EOF
  {
    "code": "btc",
    "name": "Bitcoin",
    "imageUrl": "https://static.coinpaprika.com/coin/btc-bitcoin/logo-thumb.png",
    "paprikaId": "btc-bitcoin"
  }
EOF

# Register a news feed
http POST $ENDPOINT/api/articles/feeds <<EOF
[
  "https://coinrivet.com/feed/"
]
EOF

# Process the articles
http GET $ENDPOINT/api/articles/process
