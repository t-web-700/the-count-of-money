#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
cd ..

docker container prune -f
docker volume prune -f
docker-compose up --build --detach
docker-compose logs -f server
