var supertest = require("supertest");
var should = require("should");


var server = supertest.agent("http://127.0.0.1:4000");
var id_test = "";
var id_wt = "";
var token = "";

describe("WORKINGTIME unit tests",function() {

    it("POST register user", function (done) {

        server
            .post("/api/users/register")
            .send({username: "toto", email: "toto@localhost", password: "toto"})
            .expect(201)
            .end(function (err, res) {
              if(err) done(err);
                res.status.should.equal(201)
                id_test = res.body.id
                token = res.body.token
                done()
            })
    })

})
