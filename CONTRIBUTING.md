# Contributing to the project

This file explain how to contribute to the project

## Branch Naming Guidelines

Each branch name should respect the following format:

```
feat/feat-name
```

Pull request are asked on the Master branch.

## Commit Message Guidelines

### Commit Message Format

Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:

``` HMTL
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The header is mandatory and the scope of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read on GitHub as well as in various git tools.

The footer should contain a closing reference to an issue if any.

Samples: (even more samples)

```
docs(changelog): update changelog to beta.5
```

```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

### Revert

If the commit reverts a previous commit, it should begin with revert: , followed by the header of the reverted commit. In the body it should say: This reverts commit <hash>., where the hash is the SHA of the commit being reverted.

### Type

Must be one of the following:

- __build__: Changes that affect the build system or - external dependencies (example scopes: gulp, - broccoli, npm)
- __ci__: Changes to our CI configuration files and - scripts (example scopes: Circle, BrowserStack, - SauceLabs)
- __docs__: Documentation only changes
- __feat__: A new feature
- __fix__: A bug fix
- __perf__: A code change that improves performance
- __refactor__: A code change that neither fixes a bug - nor adds a feature
- __style__: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- __test__: Adding missing tests or correcting existing tests
